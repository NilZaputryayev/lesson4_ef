﻿using System;

namespace Common.DTO.User
{
    public class NewUserDTO
    {
        public int id { get; set; }
        public int teamId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public DateTime registeredAt { get; set; }
        public DateTime birthDay { get; set; }
    }
}