﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTO.User
{
    public class UpdateUserDTO
    {
        public int id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public DateTime birthDay { get; set; }
    }
}
