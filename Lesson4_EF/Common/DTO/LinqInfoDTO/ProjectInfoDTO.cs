﻿using Common.DTO.Project;
using Common.DTO.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTO
{
    public class ProjectInfoDTO
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO LongestTaskByDescription { get; set; }
        public TaskDTO ShortestTaskByName { get; set; }
        public int CountUsersByCondition { get; set; }

        public void Print()
        {
            Console.WriteLine($"User:\n\r{Project.ToString()}");
            Console.WriteLine($"LongestTaskByDescription:\n\r{LongestTaskByDescription.ToString()}");
            Console.WriteLine($"ShortestTaskByName:\n\r{ShortestTaskByName}");
            Console.WriteLine($"CountUsersByCondition: {CountUsersByCondition}");
        }
    }
}
