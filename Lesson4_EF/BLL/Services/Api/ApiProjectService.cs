﻿using AutoMapper;
using Common.DTO.Project;
using DAL.Context;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace BLL
{
    public class ApiProjectService : BaseService, IApiProjectService
    {

        public ApiProjectService(LinqEFContext context, IMapper mapper) : base(context, mapper)
        {            
        }

        public async Task<List<ProjectDTO>> GetAsync()
        {
            return _mapper.Map<List<ProjectDTO>>(await _context.Projects.ToListAsync());
        }
        public async Task<ProjectDTO> GetByID(int id)
        {
            return _mapper.Map<ProjectDTO>(await _context.Projects.FindAsync(id));
        }
        public async Task Update(UpdateProjectDTO DTO)
        {
            var entity = await _context.Projects.FindAsync(DTO.id);

            if (entity != null)
            {
                entity.name = DTO.name;
                entity.description = DTO.description;
                entity.authorId = DTO.authorId;
                entity.deadline = DTO.deadline;
                entity.teamId = DTO.teamId;

                await _context.SaveChangesAsync();

            }
        }
        public async Task Delete(int id)
        {
            var entity = await _context.Projects.FindAsync(id);
            _context.Projects.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<ProjectDTO> Create(NewProjectDTO entity)
        {
            var model = _mapper.Map<Project>(entity);      
            
            var dto = _mapper.Map<ProjectDTO>((await _context.Projects.AddAsync(model)).Entity);

            await _context.SaveChangesAsync();

            return dto;
        }


    }
}
