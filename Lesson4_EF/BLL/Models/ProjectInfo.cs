﻿using Common.DTO.Project;
using Common.DTO.Task;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class ProjectInfo
    {
        public Project Project { get; set; }
        public Task LongestTaskByDescription { get; set; }
        public Task ShortestTaskByName { get; set; }
        public int CountUsersByCondition { get; set; }

       

    }
}
