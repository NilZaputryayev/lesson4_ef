﻿using AutoMapper;
using BLL.Models;
using Common.DTO;
using Common.DTO.Project;
using DAL.Models;

namespace BLL.MappingProfiles
{
    public sealed class UserTasksInfoProfile : Profile
    {
        public UserTasksInfoProfile()
        {
            CreateMap<UserTasksInfo, UserTasksInfoDTO>();
        }
    }
}
