﻿using AutoMapper;
using Common.DTO.User;
using DAL.Models;

namespace BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UpdateUserDTO>()
                .ReverseMap();
            CreateMap<User, UserDTO>()
                .ReverseMap();
            CreateMap<User, NewUserDTO>()
                .ReverseMap();
        }
    }
}
