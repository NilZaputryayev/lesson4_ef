﻿using Common.DTO.Project;
using DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace BLL
{
    public interface IApiProjectService
    {
        Task Delete(int id);
        Task<List<ProjectDTO>> GetAsync();
        Task<ProjectDTO> GetByID(int id);
        Task Update(UpdateProjectDTO project);
        Task<ProjectDTO> Create(NewProjectDTO project);
    }
}