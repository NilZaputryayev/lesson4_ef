﻿using BLL.Models;
using Common.DTO;
using Common.DTO.Project;
using Common.DTO.Task;
using DAL.Models;
using System.Collections.Generic;

namespace BLL
{
    public interface IApiLinqService
    {
        public Dictionary<ProjectDTO, int> GetUserTaskCountDictionaryByID(int id);
        List<TaskDTO> ListTasksWith45NameByID(int id);
        List<TaskIdName> TaskListFinished2021ByID(int id);
        List<TeamInfoDTO> GetTeamsOlder10SortedAndGrouped();
        Dictionary<string, List<TaskDTO>> GetUserListSortedWithTasks();
        UserTasksInfoDTO GetUserTasksInfo(int id);
        ProjectInfoDTO GetProjectInfo(int id);

    }
}