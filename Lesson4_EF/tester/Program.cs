﻿using BLL.Models;
using Common.DTO;
using Common.DTO.Project;
using Common.DTO.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConsoleClient
{
    class Program
    {

        static readonly LinqService linqService = new LinqService();
        static async Task Main(string[] args)
        {

            bool showMenu = true;
            while (showMenu)
            {
                showMenu = await MainMenuAsync();
                Console.WriteLine("Press any key");
                Console.ReadKey();
            }

        }
        private static async Task<bool> MainMenuAsync()
        {

            Console.Clear();

            Console.WriteLine("Select an option:");
            Console.WriteLine("1. Get count of tasks from user (id)");
            Console.WriteLine("2. Get list of tasks where name < 45 (id)");
            Console.WriteLine("3. List of finished in 2021 year tasks (id)");
            Console.WriteLine("4. List (id, name, users) from teams where users age > 10");
            Console.WriteLine("5. List of users sorted by first_name & task sorted by name desc");
            Console.WriteLine("6. Get UserInfo  (id)");
            Console.WriteLine("7. Get Project info (id)");
            Console.WriteLine("0. Exit");
            Console.Write("\r\nSelect an option: ");

            switch (Console.ReadLine())
            {
                case "1":

                    await Task1();
                    return true;
                case "2":
                    await Task2();
                    return true;
                case "3":
                    await Task3();
                    return true;
                case "4":
                    await Task4();
                    return true;
                case "5":
                    await Task5();
                    return true;
                case "6":
                    await Task6();
                    return true;
                case "7":
                    await Task7();
                    return true;
                case "0":
                    return false;
                default:
                    return true;
            }
        }
        private static async Task Task1()
        {
            if (Int32.TryParse(GetStringFromConsole("userID"), out int id))
            {                
                linqService.Endpoint = $"Linq/GetUserTaskCountDictionaryByID/{id}";
                try
                {
                    //var res = await linqService.Get<dynamic>();
                    //if (res.Count == 0)
                    //{
                    //    Console.WriteLine("No data");
                    //    return;
                    //}
                    Printer.Print(await linqService.Get<dynamic>());
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
        private static async Task Task2()
        {
            if (Int32.TryParse(GetStringFromConsole("userID"), out int id))
            {
                linqService.Endpoint = $"Linq/ListTasksWith45NameByID/{id}";
                Printer.Print((await linqService.Get<TaskDTO>()).ToList());
            }
        }
        private static async Task Task3()
        {
            if (Int32.TryParse(GetStringFromConsole("userID"), out int id))
                linqService.Endpoint = $"Linq/TaskListFinished2021ByID/{id}";
            Printer.Print((await linqService.Get<TaskIdName>()).ToList());

        }
        private static async Task Task4()
        {
            linqService.Endpoint = $"Linq/GetTeamsOlder10SortedAndGrouped";
            Printer.Print((await linqService.Get<TeamInfoDTO>()).ToList());

        }
        private static async Task Task5()
        {
            linqService.Endpoint = $"Linq/GetUserListSortedWithTasks";
            Printer.Print(await linqService.GetOne<Dictionary<string, List<TaskDTO>>>());
        }
        private static async Task Task6()
        {
            if (Int32.TryParse(GetStringFromConsole("userID"), out int id))
            {
                linqService.Endpoint = $"Linq/GetUserTasksInfo/{id}";
                (await linqService.GetOne<UserTasksInfoDTO>())?.Print();
            }
        }
        private static async Task Task7()
        {
            if (Int32.TryParse(GetStringFromConsole("projectID"), out int id))
                {
                    linqService.Endpoint = $"Linq/GetProjectInfo/{id}";
                    (await linqService.GetOne<ProjectInfoDTO>())?.Print();
                }

        } 
        private static string GetStringFromConsole(string message)
        {
            string result;
            do
            {
                Console.WriteLine($"Enter {message}:");
                result = Console.ReadLine();

            } while (string.IsNullOrEmpty(result));

            return result;
        }



    }
}
