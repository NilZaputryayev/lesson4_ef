﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class LoadService
    {
        private static readonly HttpClient client = new HttpClient();

        readonly string BaseUrl = "https://bsa21.azurewebsites.net/api/";
        public string Endpoint { get; set; }

        private static JsonSerializerSettings jsonSettings;

        public LoadService()
        {
            ConfigureClient();
        }

        public void ConfigureClient()
        {
            client.BaseAddress = new Uri(BaseUrl);
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");

            jsonSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        public async Task<List<T>> Get<T>()
        {
            List<T> data = new List<T>();

            string endpoint = Endpoint;

            try
            {
                var response = await client.GetAsync(endpoint);

                if (response.IsSuccessStatusCode)


                    data = JsonConvert.DeserializeObject<List<T>>(await response.Content.ReadAsStringAsync(), jsonSettings);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return data;


        }

        
    }
}
