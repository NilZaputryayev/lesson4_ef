﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Text.Json.Serialization;

namespace DAL.Models
{

    public class Project
    {
        [Required]
        public int id { get; set; }
        [Required]
        public int authorId { get; set; }
        public int teamId { get; set; }
        [Required]
        [MaxLength(250)]
        public string name { get; set; }
        public string description { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime deadline { get; set; }
        public IEnumerable<Task> Tasks { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }

      

    }



}
