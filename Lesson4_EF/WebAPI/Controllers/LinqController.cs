﻿using BLL;
using Common.DTO.Project;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{

    [ApiController]
    [Route("api/[controller]")]


    public class LinqController : ControllerBase
    {
        private readonly IApiLinqService _LinqService;

        public LinqController(IApiLinqService LinqService)
        {
            _LinqService = LinqService;
        }

        [HttpGet]
        [Route("GetProjectInfo/{id}")]
        public ActionResult GetProjectInfo(int id)
        {
            return Ok(_LinqService.GetProjectInfo(id));
        }

        [HttpGet]
        [Route("GetUserTasksInfo/{id}")]
        public ActionResult GetUserTasksInfo(int id)
        {
            return Ok(_LinqService.GetUserTasksInfo(id));
        }

        [HttpGet]
        [Route("GetUserListSortedWithTasks")]
        public ActionResult GetAsGetUserListSortedWithTasksnc()
        {
            return Ok(_LinqService.GetUserListSortedWithTasks());
        }

        [HttpGet]
        [Route("GetTeamsOlder10SortedAndGrouped")]
        public ActionResult GetTeamsOlder10SortedAndGrouped()
        {
            return Ok(_LinqService.GetTeamsOlder10SortedAndGrouped());
        }
         
        [HttpGet]
        [Route("ListTasksWith45NameByID/{id}")]
        public ActionResult ListTasksWith45NameByID(int id)
        {
            return Ok(_LinqService.ListTasksWith45NameByID(id));
        }

        [HttpGet]
        [Route("TaskListFinished2021ByID/{id}")]
        public ActionResult TaskListFinished2021ByID(int id)
        {
            return Ok(_LinqService.TaskListFinished2021ByID(id));
        }

        [HttpGet]
        [Route("GetUserTaskCountDictionaryByID/{id}")]
        public ActionResult GetUserTaskCountDictionaryByID(int id)
        {
            return Ok(_LinqService.GetUserTaskCountDictionaryByID(id).ToList());
        }

    }
}
