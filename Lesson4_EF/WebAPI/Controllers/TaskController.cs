﻿using BLL;
using Common.DTO.Task;
using DAL;
using DAL.Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{

    [ApiController]
    [Route("api/[controller]")]


    public class TaskController : ControllerBase
    {
        private readonly IApiTaskService _TaskService;

        public TaskController(LinqEFContext context, IApiTaskService TaskService)
        {
            _TaskService = TaskService;
        }

        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            var data = await _TaskService.Get();
            return Ok(data);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetAsync(int id)
        {
            var entity = await _TaskService.GetByID(id);

            if (entity == null) return NotFound(new { message = "not found" });

            return Ok(entity);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateAsync([FromBody] UpdateTaskDTO TaskDTO)
        {
            await _TaskService.Update(TaskDTO);
            return Ok();
        }

        [HttpPost]
        public async Task<ActionResult> CreateAsync(NewTaskDTO TaskDTO)
        {          

            return Ok(await _TaskService.Create(TaskDTO));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            await _TaskService.Delete(id);
            return NoContent();
        }
    }
}
